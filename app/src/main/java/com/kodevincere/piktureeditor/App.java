package com.kodevincere.piktureeditor;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.kodevincere.piktureeditor.dependencies.AppComponent;
import com.kodevincere.piktureeditor.dependencies.AppModule;
import com.kodevincere.piktureeditor.dependencies.DaggerAppComponent;

/**
 * Created by mE on 8/22/16.
 */
public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(this);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
