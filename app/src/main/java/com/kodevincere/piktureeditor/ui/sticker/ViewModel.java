package com.kodevincere.piktureeditor.ui.sticker;

import com.kodevincere.piktureeditor.base.ui.BaseViewModel;
import com.kodevincere.piktureeditor.model.Sticker;

import java.util.List;

/**
 * Created by mE on 9/19/16.
 */
interface ViewModel extends BaseViewModel {
    void returnClickedView(int stickerId);

    void setData(List<Sticker> stickers);
}
