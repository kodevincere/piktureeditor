package com.kodevincere.piktureeditor.ui._interface;

/**
 * Created by mE on 9/22/16.
 */
public interface ColorChangeableView {
    void onColorChanged(int color);
}
