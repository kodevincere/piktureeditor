package com.kodevincere.piktureeditor.ui.sticker;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.ui.BaseDialogFragment;

import butterknife.Bind;

/**
 * Created by mE on 9/19/16.
 */
public class DialogFragmentStickers extends BaseDialogFragment {

    @Bind(R.id.view_pager) ViewPager mPager;
    @Bind(R.id.tl_pages) TabLayout tlPages;

    private ScreenSlidePagerAdapter mPagerAdapter;
    private String[] categories = {FragmentStickerPage.STICKER_CATEGORY_SMILEY,
            FragmentStickerPage.STICKER_CATEGORY_FACE};

    @Override
    public int getViewLayout() {
        return R.layout.fragment_stickers;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(getContext(), R.color.black_overlay)));
        return dialog;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTabs();
    }

    private void initTabs() {
        mPagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        tlPages.setupWithViewPager(mPager);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentStickerPage.newInstance(categories[position]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return categories[position].toUpperCase();
        }

        @Override
        public int getCount() {
            return categories.length;
        }
    }

}
