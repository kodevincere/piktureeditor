package com.kodevincere.piktureeditor.ui.sticker.recyclerview;

import android.net.Uri;
import android.view.View;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.view.SimpleDraweeView;
import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.recycleview.BaseViewHolder;
import com.kodevincere.piktureeditor.base.recycleview.OnActionAdapter;
import com.kodevincere.piktureeditor.model.Sticker;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by mE on 9/19/16.
 */
class StickerViewHolder extends BaseViewHolder<Sticker> {

    @Bind(R.id.my_image_view) SimpleDraweeView simpleDraweeView;

    public StickerViewHolder(View itemView) {
        super(itemView);
    }

    public StickerViewHolder(View itemView, OnActionAdapter action) {
        super(itemView, action);
    }

    @Override
    public void bindHolder(Sticker item) {
        super.bindHolder(item);

        Uri uri = new Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(String.valueOf(item.getStickerId()))
                .build();

        simpleDraweeView.setImageURI(uri);
    }

    @Override
    public void clean() {

    }

    @OnClick(R.id.ll_root)
    void onClick(View view) {
        dispatchClick(view);
    }
}
