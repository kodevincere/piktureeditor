package com.kodevincere.piktureeditor.ui.pictureeditor;

import com.kodevincere.piktureeditor.dependencies.ActivityScope;
import com.kodevincere.piktureeditor.dependencies.AppComponent;

import dagger.Component;

/**
 * Created by mE on 7/12/16.
 */
@ActivityScope
@Component(
        modules = PiktureEditorActivityModule.class,
        dependencies = {AppComponent.class}
)
public interface PiktureEditorActivityComponent {
    void inject(PiktureEditorActivity piktureEditorActivity);
}
