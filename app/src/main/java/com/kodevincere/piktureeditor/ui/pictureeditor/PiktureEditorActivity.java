package com.kodevincere.piktureeditor.ui.pictureeditor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kodevincere.piktureeditor.App;
import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.launcher.LauncherViews;
import com.kodevincere.piktureeditor.base.ui.BaseActivity;
import com.kodevincere.piktureeditor.customviews.sticker.StickerLayout;
import com.kodevincere.piktureeditor.customviews.sticker.StickerSnapchatText;
import com.kodevincere.piktureeditor.customviews.sticker.StickerSubsamplingScaleImage;
import com.kodevincere.piktureeditor.model.Size;
import com.kodevincere.piktureeditor.subscaleview.ImageSource;
import com.kodevincere.piktureeditor.ui.sticker.DialogFragmentStickers;
import com.rtugeek.android.colorseekbar.ColorSeekBar;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by mE on 8/22/16.
 */
public class PiktureEditorActivity extends BaseActivity<Presenter> implements ViewModel {

    public static final String KEY_STICKER = "sticker";
    public static final String PICTURE_SOURCE = "picture_source";
    public static final String PICTURE_DESTINATION = "picture_destination";

    public static final int CODE_EDIT_PICTURE = 1;
    private static final int NORMAL_SIZE = 1;
    private static final int DOUBLE_SIZE = 2;

    @Bind(R.id.tv_loading) TextView tvLoading;
    @Bind(R.id.rl_picture) RelativeLayout rlPicture;
    @Bind(R.id.colorSlider) ColorSeekBar colorSeekBar;
    @Bind(R.id.crop_image_view) CropImageView cropImageView;
    @Bind(R.id.custom_image_view) StickerSubsamplingScaleImage customImageView;

    @Bind(R.id.btn_save) Button saveButton;
    @Bind(R.id.btn_crop) Button cropButton;
    @Bind(R.id.btn_undo) Button undoButton;
    @Bind(R.id.btn_redo) Button redoButton;
    @Bind(R.id.btn_write) Button writeButton;
    @Bind(R.id.btn_add_text) Button addTextButton;
    @Bind(R.id.btn_add_sticker) Button addStickerButton;
    @Bind(R.id.btn_accept_crop) Button acceptCropButton;
    @Bind(R.id.btn_accept_write) Button acceptWriteButton;
    @Bind(R.id.btn_delete_sticker) Button deleteStickerButton;

    private DialogFragmentStickers dialogFragment;
    private Size deleteStickerButtonSize = new Size(0, 0);;

    @Override
    public int getViewLayout() {
        return R.layout.activity_pikture_editor;
    }

    @Override
    public void startPresenter() {
        super.startPresenter();

        App app = ((App) getApplication());

        DaggerPiktureEditorActivityComponent.builder()
                .appComponent(app.getAppComponent())
                .piktureEditorActivityModule(new PiktureEditorActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.handleIntent(getIntent());
        initColorPicker();
    }

    @Override
    public void showNewView(StickerLayout draggableView) {

        centerView(draggableView);

        customImageView.addDraggableView(draggableView);
        rlPicture.addView(draggableView);
    }

    @Override
    public void showText(StickerSnapchatText stickerSnapchatText) {

        centerView(stickerSnapchatText);

        customImageView.addText(stickerSnapchatText);
        rlPicture.addView(stickerSnapchatText);
    }

    private void centerView(StickerLayout view){
        ViewGroup.LayoutParams stickerLayoutParams = view.getLayoutParams();

        int yPosition = (rlPicture.getHeight() - stickerLayoutParams.height) / 2;
        int xPosition = (rlPicture.getWidth() - stickerLayoutParams.width) / 2;

        view.updateXPosition(xPosition);
        view.updateYPosition(yPosition);
    }

    @Override
    public void showPicture(ImageSource uri) {
        customImageView.setImage(uri);
    }

    @Override
    public void showPictureToCrop(Uri imageSource) {
        cropImageView.setImageUriAsync(imageSource);
    }

    @Override
    public void showPictureToCrop(Bitmap bitmap) {
        cropImageView.setImageBitmap(bitmap);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(PiktureEditorActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {
        tvLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        tvLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void enableViewWithId(int viewId) {
        findViewById(viewId).setEnabled(true);
    }

    @Override
    public void enableStickerDeletion() {
        deleteStickerButton.setText("E");
    }

    @Override
    public void stickerWithinDeleteView() {

        if(deleteStickerButtonSize.getWidth() == 0 && deleteStickerButtonSize.getHeight() == 0)
            extractDeleteViewSize();

        changeDeleteViewSize(DOUBLE_SIZE);

        Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        v.vibrate(50);
    }

    @Override
    public void stickerOutsideDeleteView() {
        if(deleteStickerButtonSize.getWidth() == 0 && deleteStickerButtonSize.getHeight() == 0)
            extractDeleteViewSize();

        changeDeleteViewSize(NORMAL_SIZE);
    }

    @Override
    public void disableViewWithId(int viewId) {
        findViewById(viewId).setEnabled(false);
    }

    @Override
    public void disableStickerDeletion() {
        deleteStickerButton.setText("D");
    }

    @Override
    public void setVisibleViewWithId(int viewId) {
        findViewById(viewId).setVisibility(View.VISIBLE);
    }

    @Override
    public void setInvisibleViewWithId(int viewId) {
        findViewById(viewId).setVisibility(View.INVISIBLE);
    }

    @Override
    public void clearCrop() {
        cropImageView.clearImage();
        cropImageView.clearAspectRatio();
        cropImageView.setFixedAspectRatio(false);
    }

    @Override
    public void finishActivityWithResult(Intent i, int resultCode) {
        setResult(resultCode, i);
        finish();
    }

    @Override
    public Rect getDeleteViewRect() {
        int[] xy = {0, 0};
        cropImageView.getLocationOnScreen(xy);

        return new Rect(xy[0] + deleteStickerButton.getLeft(), xy[1] + deleteStickerButton.getTop(),
                xy[0] + deleteStickerButton.getRight(), xy[1] + deleteStickerButton.getBottom());
    }


    @OnClick(R.id.btn_add_sticker)
    void addStickerClicked(View v) {
        dialogFragment = new DialogFragmentStickers();
        dialogFragment.show(getSupportFragmentManager(), "TAG");
    }

    @OnClick(R.id.btn_save)
    void saveClicked(View v) {
        presenter.onSavePicture(customImageView);
    }

    @OnClick(R.id.btn_crop)
    void cropClicked(View v) {
        presenter.onStartCropping(customImageView);
    }

    @OnClick(R.id.btn_write)
    void writeClicked(View v) {
        presenter.onWriteInPicture(customImageView);
    }

    @OnClick(R.id.btn_accept_crop)
    void acceptCrop(View v) {
        presenter.onPictureCropped(cropImageView.getCroppedImage());
    }

    @OnClick(R.id.btn_accept_write)
    void acceptWrite(View v) {
        presenter.onPictureWrote(customImageView);
    }

    @OnClick(R.id.btn_undo)
    void undoClicked(View v) {
        presenter.undo();
    }

    @OnClick(R.id.btn_redo)
    void redo(View v) {
        presenter.redo();
    }

    @OnClick(R.id.btn_add_text)
    void addText(View v) {
        presenter.onAddTextSticker();
    }

    @Override
    public void setResultCode(LauncherViews launcherViews, int resultCode) {
        super.setResultCode(launcherViews, resultCode);
        dialogFragment.dismiss();
        Bundle bundle = getBundleLauncher(LauncherViews.InitialActivity);

        if (resultCode == Activity.RESULT_OK) {
            if (bundle != null) {
                int i = bundle.getInt(KEY_STICKER);
                presenter.onAddImageSticker(i);
            }
        }
    }

    private void initColorPicker() {
        colorSeekBar.setMaxValue(100);
        colorSeekBar.setColors(R.array.material_colors);
        customImageView.setWriteColor(colorSeekBar.getColor());
        colorSeekBar.setOnColorChangeListener((colorBarValue, alphaBarValue, color) ->
                presenter.onColorChanged(color));
    }

    private void extractDeleteViewSize() {
        deleteStickerButtonSize.setWidth(deleteStickerButton.getWidth());
        deleteStickerButtonSize.setHeight(deleteStickerButton.getHeight());
    }

    private void changeDeleteViewSize(int sizeMuiliplier) {
        ViewGroup.LayoutParams layoutParams = deleteStickerButton.getLayoutParams();
        layoutParams.width = deleteStickerButtonSize.getWidth() * sizeMuiliplier;
        layoutParams.height = deleteStickerButtonSize.getHeight() * sizeMuiliplier;

        deleteStickerButton.setLayoutParams(layoutParams);
    }

}
