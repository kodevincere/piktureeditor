package com.kodevincere.piktureeditor.ui.pictureeditor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.mvp.BasePresenter;
import com.kodevincere.piktureeditor.base.references.FromInteractor;
import com.kodevincere.piktureeditor.base.references.FromView;
import com.kodevincere.piktureeditor.customviews.sticker.StickerImageView;
import com.kodevincere.piktureeditor.customviews.sticker.StickerLayout;
import com.kodevincere.piktureeditor.customviews.sticker.StickerSnapchatText;
import com.kodevincere.piktureeditor.customviews.sticker.StickerSnapchatText.TextEditEventListener;
import com.kodevincere.piktureeditor.customviews.sticker.StickerSubsamplingScaleImage;
import com.kodevincere.piktureeditor.fileutils.ImageHelper;
import com.kodevincere.piktureeditor.subscaleview.ImageSource;
import com.kodevincere.piktureeditor.ui._interface.ColorChangeableView;

import java.io.File;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

import static com.kodevincere.piktureeditor.customviews.sticker.StickerLayout.StickerDragListener;

/**
 * Created by mE on 8/22/16.
 */
class Presenter extends BasePresenter<ViewModel> implements PresenterModel, TextEditEventListener,
        StickerDragListener{

    //Min size of saving picture
    private final int MIN_WIDTH = 1024;
    private final int MIN_HEIGHT = 1024;

    // Actions codes
    private final int ACTION_NOTHING = 0;
    private final int ACTION_CROP_START = 1;
    private final int ACTION_CROP_DONE = 2;
    private final int ACTION_WRITE_START = 3;
    private final int ACTION_WRITE_DONE = 4;
    private final int ACTION_FINISH = 100;

    // Source and destination path files
    private String sourceFilePath;
    private String destinationFilePath;

    // Temp files names
    private final String JPEG_EXTENSION = ".jpg";
    private final String TEMP_LOCATION = "/storage/emulated/0/temp";
    private int tempCounter = 0;

    // Important views rect
    private Rect deleteButtonRect = null;

    private boolean stickerBeingDragged = false;

    private ColorChangeableView actualColorChangeableView;

    private Interactor interactor;

    @Inject
    public Presenter(ViewModel viewModel, Interactor interactor) {
        super(viewModel);
        this.interactor = interactor;
        this.interactor.setPresenterModel(this);
    }

    public void handleIntent(Intent intent) {
        if (intent != null) {
            Bundle b = intent.getExtras();
            if (b != null) {
                sourceFilePath = b.getString(PiktureEditorActivity.PICTURE_SOURCE);
                destinationFilePath = b.getString(PiktureEditorActivity.PICTURE_DESTINATION);
                onLoadPicture();
            }
        }
    }

    @Override
    public void onTextEditionStarted(StickerSnapchatText stickerSnapchatText) {
        this.actualColorChangeableView = stickerSnapchatText;
        willStartEditingText();
    }

    @Override
    public void onTextEditionFinished(StickerSnapchatText stickerSnapchatText) {
        finishedEditingText(stickerSnapchatText);
    }

    @Override
    public void onStickerIsBeingDragged(StickerLayout stickerLayout, MotionEvent motionEvent) {
        extractViewRects();
        evalMotionWithViewRects(motionEvent);

        if(!stickerBeingDragged) {
            stickerBeingDragged = true;
            viewModel.enableStickerDeletion();
        }
    }

    @Override
    public void onStickerIsReleased(StickerLayout stickerLayout, MotionEvent motionEvent) {

        if(canDelete(motionEvent.getRawX(), motionEvent.getRawY()))
            stickerLayout.delete();

        viewModel.disableStickerDeletion();
        viewModel.stickerOutsideDeleteView();

        stickerBeingDragged = false;
    }

    @Override
    @FromInteractor
    public void fileCopiedSuccessfully(boolean success, String source, String destination) {
        viewModel.hideLoader();

        if (success)
            viewModel.showPicture(ImageSource.bitmap(new ImageHelper().loadReducedBitmap(source,
                    MIN_WIDTH, MIN_HEIGHT)));
        else
            viewModel.showToast("No fue exitosa la copia");
    }

    @Override
    @FromInteractor
    public void bitmapSavedSuccessfully(boolean success, String path, int action) {
        viewModel.hideLoader();

        switch (action) {
            case ACTION_FINISH:
                saveAndFinishSuccesfully(success, path);
        }

        if (!success)
            viewModel.showToast("No se pudo guardar la imagen");
    }

    @FromView
    public void onAddImageSticker(int stickerResourceId) {
        final StickerImageView stickerImageView = new StickerImageView(context);
        stickerImageView.setImageResource(stickerResourceId);
        stickerImageView.setStickerDragListener(this);
        viewModel.showNewView(stickerImageView);
    }

    @FromView
    public void onAddTextSticker() {
        final StickerSnapchatText text = new StickerSnapchatText(context);

        text.setEventListener(this);
        text.setStickerDragListener(this);
        text.setText("Text");
        text.setOnClickListener(view -> {
            text.startEditingText();
        });

        viewModel.showText(text);
    }

    @FromView
    public void onLoadPicture() {
        viewModel.showLoader();
        interactor.copyFileToPath(sourceFilePath, getActualFilePath());
    }

    @FromView
    public void onWriteInPicture(final StickerSubsamplingScaleImage stickerSubsamplingScaleImage) {

        this.actualColorChangeableView = stickerSubsamplingScaleImage;

        addStickersToBitmap(1, stickerSubsamplingScaleImage, ACTION_WRITE_START)
                .doOnCompleted(() -> {
                    stickerSubsamplingScaleImage.setZoomEnabled(false);
                    stickerSubsamplingScaleImage.setCanWrite(true);
                    stickerSubsamplingScaleImage.resetScaleAndCenter();
                    willStartWritingInPicture();
                })
                .subscribe();
    }

    @FromView
    public void onSavePicture(final StickerSubsamplingScaleImage stickerSubsamplingScaleImage) {
        addAllViewsAndSaveInLocation(stickerSubsamplingScaleImage, destinationFilePath,
                ACTION_FINISH);
    }

    @FromView
    public void onStartCropping(final StickerSubsamplingScaleImage stickerSubsamplingScaleImage) {
        final Bitmap bitmap = stickerSubsamplingScaleImage.getMyBitmap();
        addStickersToBitmap(1, stickerSubsamplingScaleImage, ACTION_CROP_START)
                .doOnCompleted(() -> willStartCroppingPicture(bitmap))
                .subscribe();
    }

    @FromView
    public void onPictureCropped(Bitmap croppedImage) {
        finishedCroppingPicture(croppedImage);
    }

    @FromView
    public void onPictureWrote(StickerSubsamplingScaleImage stickerSubsamplingScaleImage) {
        stickerSubsamplingScaleImage.setZoomEnabled(true);
        stickerSubsamplingScaleImage.setCanWrite(false);
        finishedWritingInPicture();
    }

    @FromView
    public void undo() {
        final String previousFilePath = getTempFilePathWithCounter(tempCounter - 1);
        final File previousFile = new File(previousFilePath);
        if (previousFile.exists()) {
            viewModel.showPicture(ImageSource.uri(previousFilePath));
            tempCounter--;
        } else
            viewModel.showToast("El archivo no existe");
    }

    @FromView
    public void redo() {
        final String nextFilePath = getTempFilePathWithCounter(tempCounter + 1);
        final File nextFile = new File(nextFilePath);

        if (nextFile.exists()) {
            viewModel.showPicture(ImageSource.uri(nextFilePath));
            tempCounter++;
        } else
            viewModel.showToast("El archivo no existe");

    }

    @FromView
    public void onColorChanged(int color) {
        actualColorChangeableView.onColorChanged(color);
    }

    private void willStartEditingText() {
        viewModel.setVisibleViewWithId(R.id.colorSlider);
    }

    private void willStartCroppingPicture(Bitmap bitmap) {
        viewModel.disableViewWithId(R.id.btn_crop);
        viewModel.disableViewWithId(R.id.btn_write);
        viewModel.disableViewWithId(R.id.btn_undo);
        viewModel.disableViewWithId(R.id.btn_redo);
        viewModel.disableViewWithId(R.id.btn_add_sticker);
        viewModel.disableViewWithId(R.id.btn_add_text);
        viewModel.enableViewWithId(R.id.btn_accept_crop);
        viewModel.showPictureToCrop(bitmap);
        viewModel.setInvisibleViewWithId(R.id.custom_image_view);
        viewModel.setVisibleViewWithId(R.id.crop_image_view);
    }

    private void willStartWritingInPicture() {
        viewModel.disableViewWithId(R.id.btn_write);
        viewModel.disableViewWithId(R.id.btn_crop);
        viewModel.disableViewWithId(R.id.btn_undo);
        viewModel.disableViewWithId(R.id.btn_redo);
        viewModel.disableViewWithId(R.id.btn_add_sticker);
        viewModel.disableViewWithId(R.id.btn_add_text);
        viewModel.enableViewWithId(R.id.btn_accept_write);
        viewModel.setVisibleViewWithId(R.id.colorSlider);
    }

    private void finishedEditingText(StickerSnapchatText stickerSnapchatText) {

        checkTextNewVisibility(stickerSnapchatText);

        viewModel.setInvisibleViewWithId(R.id.colorSlider);
    }

    private void finishedWritingInPicture() {
        viewModel.enableViewWithId(R.id.btn_crop);
        viewModel.enableViewWithId(R.id.btn_write);
        viewModel.enableViewWithId(R.id.btn_undo);
        viewModel.enableViewWithId(R.id.btn_redo);
        viewModel.enableViewWithId(R.id.btn_add_sticker);
        viewModel.enableViewWithId(R.id.btn_add_text);
        viewModel.disableViewWithId(R.id.btn_accept_write);
        viewModel.setInvisibleViewWithId(R.id.colorSlider);
        viewModel.setVisibleViewWithId(R.id.custom_image_view);
    }

    private void finishedCroppingPicture(Bitmap bitmap) {
        viewModel.enableViewWithId(R.id.btn_crop);
        viewModel.enableViewWithId(R.id.btn_write);
        viewModel.enableViewWithId(R.id.btn_undo);
        viewModel.enableViewWithId(R.id.btn_redo);
        viewModel.enableViewWithId(R.id.btn_add_sticker);
        viewModel.enableViewWithId(R.id.btn_add_text);
        viewModel.disableViewWithId(R.id.btn_accept_crop);
        viewModel.setInvisibleViewWithId(R.id.crop_image_view);
        viewModel.setVisibleViewWithId(R.id.custom_image_view);
        viewModel.showPicture(ImageSource.bitmap(bitmap));
        viewModel.clearCrop();
    }

    private void saveAndFinishSuccesfully(boolean success, String path) {
        if (success)
            finishEdit();
        else
            viewModel.showToast("No se pudo guardar satisfactoriamente");
    }

    private void saveBitmap(Bitmap bitmap, String filePath, int action) {
        viewModel.showLoader();
        interactor.saveBitmap(bitmap, filePath, action);
    }

    private void addAllViewsAndSaveInLocation(final StickerSubsamplingScaleImage customImageView,
                                              final String fileLocation, final int action) {
        Bitmap mutableBitmap = customImageView.getMyBitmap();

        customImageView.drawAll(1)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> {
                    // Must be done after finishing because stickers are not always added
                    // by the user
                    saveBitmap(mutableBitmap, fileLocation, action);
                })
                .subscribe();
    }

    private Observable<StickerLayout> addStickersToBitmap(final int sampleSize,
                                                          final StickerSubsamplingScaleImage stickerSubsamplingScaleImage,
                                                          final int action) {

        return stickerSubsamplingScaleImage.drawStickers(sampleSize)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> {
                    stickerSubsamplingScaleImage.clearStickers();
                    switch (action) {
                        case ACTION_WRITE_START:
                            break;
                        case ACTION_CROP_START:
                            stickerSubsamplingScaleImage.setVisibility(View.INVISIBLE);
                            break;
                    }
                });
    }

    private String getActualFilePath() {
        return getTempFilePathWithCounter(tempCounter);
    }

    private String getTempFilePathWithCounter(int desiredTempCounter) {
        return TEMP_LOCATION.concat(String.valueOf(desiredTempCounter)).concat(JPEG_EXTENSION);
    }

    private void extractViewRects() {
        if(deleteButtonRect == null)
            deleteButtonRect = viewModel.getDeleteViewRect();
    }

    private void evalMotionWithViewRects(MotionEvent motionEvent) {
        float x = motionEvent.getRawX();
        float y = motionEvent.getRawY();

        evalMotionWithDeleteViewRect(x, y);
    }

    private void evalMotionWithDeleteViewRect(float x, float y) {
        if(canDelete(x,y))
            viewModel.stickerWithinDeleteView();
        else
            viewModel.stickerOutsideDeleteView();
    }

    private boolean canDelete(float x, float y){
        return deleteButtonRect.contains((int) x, (int) y);
    }

    private void finishEdit() {
        deleteTempFiles();
        final Intent i = new Intent();
        i.putExtra(PiktureEditorActivity.PICTURE_DESTINATION, destinationFilePath);
        viewModel.finishActivityWithResult(i, Activity.RESULT_OK);
    }

    private void deleteTempFiles() {
        for (int i = 0; i <= tempCounter; i++) {
            final String filePath = getTempFilePathWithCounter(i);
            interactor.deleteFile(filePath);
        }
    }

    private void checkTextNewVisibility(StickerSnapchatText stickerSnapchatText) {
        String text = stickerSnapchatText.getText();

        if(text.trim().length() == 0)
            stickerSnapchatText.delete();

    }

}