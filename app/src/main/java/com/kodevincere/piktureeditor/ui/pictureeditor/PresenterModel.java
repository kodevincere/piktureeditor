package com.kodevincere.piktureeditor.ui.pictureeditor;

import com.kodevincere.piktureeditor.base.mvp.BasePresenterModel;

/**
 * Created by mE on 8/22/16.
 */
interface PresenterModel extends BasePresenterModel {
    void fileCopiedSuccessfully(boolean success, String source, String destination);

    void bitmapSavedSuccessfully(boolean success, String path, int action);
}
