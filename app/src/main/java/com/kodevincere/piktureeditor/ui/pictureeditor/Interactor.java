package com.kodevincere.piktureeditor.ui.pictureeditor;

import android.content.Context;
import android.graphics.Bitmap;

import com.kodevincere.piktureeditor.base.mvp.BaseInteractor;
import com.kodevincere.piktureeditor.fileutils.FileOperations;

import java.io.File;
import java.io.FileOutputStream;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mE on 8/22/16.
 */
class Interactor extends BaseInteractor<PresenterModel> {

    @Inject
    public Interactor(Context context) {
        super(context);
    }

    public void copyFileToPath(final String source, final String destination) {
        copyFileToPathObservable(source, destination)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    presenterModel.fileCopiedSuccessfully(success, source, destination);
                });
    }

    public void saveBitmap(final Bitmap bitmap, final String path, final int action) {
        saveBitmapObservable(bitmap, path)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    presenterModel.bitmapSavedSuccessfully(success, path, action);
                });

    }

    public Observable<Boolean> copyFileToPathObservable(final String source, final String destination) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                final boolean success = FileOperations.copyFile(new File(source), new File(destination));
                subscriber.onNext(success);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<Boolean> saveBitmapObservable(final Bitmap bitmap, final String path) {
        return Observable.create((Observable.OnSubscribe<Boolean>) subscriber -> {
            final File f = new File(path);
            boolean success = true;
            try {
                final FileOutputStream fOS = new FileOutputStream(f);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fOS);
                fOS.flush();
                fOS.close();
            } catch (Exception ex) {
                success = false;
                ex.printStackTrace();
            }
            subscriber.onNext(success);
            subscriber.onCompleted();
        });
    }

    public boolean deleteFile(String filePath) {
        return new File(filePath).delete();
    }
}
