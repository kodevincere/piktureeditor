package com.kodevincere.piktureeditor.ui.sticker;

import android.content.res.Resources;
import android.os.Bundle;

import com.kodevincere.piktureeditor.base.mvp.BasePresenter;
import com.kodevincere.piktureeditor.base.recycleview.ActionPack;
import com.kodevincere.piktureeditor.base.references.FromView;
import com.kodevincere.piktureeditor.model.Sticker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mE on 9/19/16.
 */
class Presenter extends BasePresenter<ViewModel> {

    private List<Sticker> stickers = new ArrayList<>();
    private String category;

    public Presenter(ViewModel viewModel) {
        super(viewModel);
    }

    @FromView
    public void onReceivedArguments(Bundle arguments) {
        if (arguments != null && arguments.containsKey(FragmentStickerPage.STICKER_CATEGORY))
            category = arguments.getString(FragmentStickerPage.STICKER_CATEGORY);

        if (category.trim().length() > 0)
            loadCategoryStickers();
    }

    @FromView
    public void onActionAdapter(ActionPack actionPack) {
        Sticker sticker = (Sticker) actionPack.getItem();
        viewModel.returnClickedView(sticker.getStickerId());
    }

    private void loadCategoryStickers() {
        int stickerId = -1;
        int counter = 1;

        Resources res = context.getResources();

        while (stickerId != 0) {
            stickerId = res.getIdentifier(category.concat("_").concat(String.valueOf(counter)),
                    "drawable", context.getPackageName());

            counter++;

            if (stickerId != 0)
                stickers.add(new Sticker(stickerId));
        }

        viewModel.setData(stickers);
    }

}
