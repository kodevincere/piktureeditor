package com.kodevincere.piktureeditor.ui.pictureeditor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mE on 7/12/16.
 */
@Module
public class PiktureEditorActivityModule {

    private ViewModel viewModel;

    public PiktureEditorActivityModule(ViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Provides
    ViewModel providesViewModel() {
        return viewModel;
    }
}
