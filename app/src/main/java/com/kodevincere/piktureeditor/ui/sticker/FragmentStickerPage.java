package com.kodevincere.piktureeditor.ui.sticker;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.launcher.LauncherViews;
import com.kodevincere.piktureeditor.base.recycleview.ActionPack;
import com.kodevincere.piktureeditor.base.recycleview.OnActionAdapter;
import com.kodevincere.piktureeditor.base.ui.BaseFragment;
import com.kodevincere.piktureeditor.model.Sticker;
import com.kodevincere.piktureeditor.ui.pictureeditor.PiktureEditorActivity;
import com.kodevincere.piktureeditor.ui.sticker.recyclerview.StickerAdapter;

import java.util.List;

import butterknife.Bind;

/**
 * Created by mE on 9/19/16.
 */
public class FragmentStickerPage extends BaseFragment<Presenter> implements ViewModel, OnActionAdapter {

    public static final String STICKER_CATEGORY = "category";
    public static final String STICKER_CATEGORY_SMILEY = "smiley";
    public static final String STICKER_CATEGORY_FACE = "face";

    @Bind(R.id.rv_stickers) RecyclerView rvStickers;

    private StickerAdapter adapter;

    public static FragmentStickerPage newInstance(String category) {

        Bundle args = new Bundle();
        args.putString(STICKER_CATEGORY, category);

        FragmentStickerPage fragment = new FragmentStickerPage();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getViewLayout() {
        return R.layout.fragment_screen_slide_page;
    }

    @Override
    public Presenter startPresenter() {
        return new Presenter(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecycler();
        presenter.onReceivedArguments(getArguments());
    }

    private void initRecycler() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 5);

        adapter = new StickerAdapter();
        adapter.addActionAdapter(this);

        rvStickers.setLayoutManager(gridLayoutManager);
        rvStickers.setAdapter(adapter);
    }

    @Override
    public void onActionAdapter(ActionPack actionPack) {
        presenter.onActionAdapter(actionPack);
    }

    @Override
    public void returnClickedView(int stickerId) {
        final Bundle bundle = new Bundle();

        bundle.putInt(PiktureEditorActivity.KEY_STICKER, stickerId);

        onManageNavigation.onSaveExtra(LauncherViews.InitialActivity, bundle);
        onManageNavigation.setResultCode(LauncherViews.InitialActivity, Activity.RESULT_OK);
    }

    @Override
    public void setData(List<Sticker> stickers) {
        adapter.setData(stickers);
    }
}