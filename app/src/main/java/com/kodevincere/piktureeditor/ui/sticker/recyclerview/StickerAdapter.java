package com.kodevincere.piktureeditor.ui.sticker.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.recycleview.BaseAdapter;
import com.kodevincere.piktureeditor.model.Sticker;

import java.util.List;

/**
 * Created by mE on 9/19/16.
 */
public class StickerAdapter extends BaseAdapter<StickerViewHolder> {

    private List<Sticker> stickerList;

    public void setData(List<Sticker> stickerList) {
        this.stickerList = stickerList;
        notifyDataSetChanged();
    }

    @Override
    public StickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_sticker, parent, false);
        StickerViewHolder holder = new StickerViewHolder(view, actionAdapter);
        return holder;
    }

    @Override
    public void onBindViewHolder(StickerViewHolder holder, int position) {
        holder.bindHolder(stickerList.get(position));
    }

    @Override
    public int getItemCount() {
        return stickerList == null ? 0 : stickerList.size();
    }

}
