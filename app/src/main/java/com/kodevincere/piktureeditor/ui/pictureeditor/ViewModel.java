package com.kodevincere.piktureeditor.ui.pictureeditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;

import com.kodevincere.piktureeditor.base.ui.BaseViewModel;
import com.kodevincere.piktureeditor.customviews.sticker.StickerLayout;
import com.kodevincere.piktureeditor.customviews.sticker.StickerSnapchatText;
import com.kodevincere.piktureeditor.subscaleview.ImageSource;

/**
 * Created by mE on 8/22/16.
 */
interface ViewModel extends BaseViewModel {
    void showNewView(StickerLayout view);

    void showText(StickerSnapchatText text);

    void showPicture(ImageSource imageSource);

    void showPictureToCrop(Uri imageSource);

    void showPictureToCrop(Bitmap bitmap);

    void showToast(String message);

    void showLoader();

    void hideLoader();

    void enableViewWithId(int viewId);

    void enableStickerDeletion();

    void stickerWithinDeleteView();

    void stickerOutsideDeleteView();

    void disableViewWithId(int viewId);

    void disableStickerDeletion();

    void setVisibleViewWithId(int viewId);

    void setInvisibleViewWithId(int viewId);

    void clearCrop();

    void finishActivityWithResult(Intent i, int resultCode);

    Rect getDeleteViewRect();

}
