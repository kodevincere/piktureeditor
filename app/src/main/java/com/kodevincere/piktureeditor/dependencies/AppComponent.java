package com.kodevincere.piktureeditor.dependencies;

import android.app.Application;
import android.content.Context;

import dagger.Component;

/**
 * Created by mE on 7/12/16.
 */

@Component(
        modules = {AppModule.class}
)
public interface AppComponent {

    Application providesApplication();

    Context providesContext();

}
