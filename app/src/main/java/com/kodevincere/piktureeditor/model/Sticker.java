package com.kodevincere.piktureeditor.model;

/**
 * Created by mE on 9/19/16.
 */
public class Sticker {

    private int stickerId;

    public Sticker(int stickerId) {
        this.stickerId = stickerId;
    }

    public Sticker setStickerId(int stickerId) {
        this.stickerId = stickerId;
        return this;
    }

    public int getStickerId() {
        return stickerId;
    }

}
