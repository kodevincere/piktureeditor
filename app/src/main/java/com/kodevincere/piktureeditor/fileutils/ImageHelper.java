package com.kodevincere.piktureeditor.fileutils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by alphonselric
 */
public class ImageHelper {

    private static final int MAX_SIZE = 2048;
    private static final String EMPTY_PATH = "";

    private String imagePath;
    private String destinationPath;

    public ImageHelper() {
    }

    public ImageHelper(String imagePath, String destinationPath) {
        this.imagePath = imagePath;
        this.destinationPath = destinationPath;
    }

    public String compressImage() {
        Options options = getPictureOptions(imagePath);

        int imageWidth = options.outWidth;
        int imageHeight = options.outHeight;
        int larger = imageHeight;

        if (imageWidth > imageHeight)
            larger = imageWidth;

        if (larger > MAX_SIZE) {
            int aux = larger / 1000;
            imageWidth = (imageWidth / aux) - 50; //To get the most little image possible using calculateInSampleSize
            imageHeight = (imageHeight / aux) - 50;
        }

        options.inSampleSize = calculateInSampleSize(options, imageWidth, imageHeight, true);
        options.inJustDecodeBounds = false;

        Bitmap bm = BitmapFactory.decodeFile(imagePath, options);

        boolean compressed = reduceImageQuality(bm);

        moveExifOrientation(imagePath, destinationPath);

        if (compressed)
            return destinationPath;
        else
            return EMPTY_PATH;

    }

    public Options getDesiredOptionsSampleSize(String filePath, int reqWidth, int reqHeight) {
        Options opt = getPictureOptions(filePath);
        int sampleSize = calculateInSampleSize(opt, reqWidth, reqHeight, true);
        opt.inSampleSize = sampleSize;
        return opt;
    }


    public boolean reduceImageQuality(Bitmap bm) {
        return saveCompressedImage(bm, 90);
    }

    public boolean saveImage(Bitmap bm) {
        return saveCompressedImage(bm, 100);
    }

    private boolean saveCompressedImage(Bitmap bm, int quality) {
        boolean compressed = false;

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(destinationPath);

            compressed = bm.compress(Bitmap.CompressFormat.JPEG, quality, fileOutputStream);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return compressed;
    }

    private Options getPictureOptions(String imagePath) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);

        return options;
    }

    private int calculateInSampleSize(Options options, int reqWidth, int reqHeight, boolean compress) {
        if (compress)
            return calculateInSampleSizeToCompress(options, reqWidth, reqHeight);
        else
            return calculateInSampleSizeToShow(options, reqWidth, reqHeight);

    }

    private int calculateInSampleSizeToCompress(Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight / 2;
        final int width = options.outWidth / 2;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((height / inSampleSize) > reqHeight && (width / inSampleSize) > reqWidth) { // Check the picture size is not bigger than required size
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private int calculateInSampleSizeToShow(Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight / 2;
        final int width = options.outWidth / 2;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (((width / inSampleSize) > 4096) || ((height / inSampleSize) > 4096) || // Check both sizes are less than 4096 (Max defined by Android)
                    (height / inSampleSize) > reqHeight && (width / inSampleSize) > reqWidth) { // Check the picture size is not bigger than required size
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Bitmap loadReducedBitmap(final String filePath, final int reqWidth, final int reqHeight) {

        Options options = getPictureOptions(filePath);
        int sampleSize = calculateInSampleSize(options, reqWidth, reqHeight, false);
        options.inSampleSize = sampleSize;

        options.inJustDecodeBounds = false;
        options.inMutable = true;

        return BitmapFactory.decodeFile(filePath, options);

    }

    public Bitmap loadBitmapWithCorrectOrientation(Bitmap bitmap, String filePath) {
        try {
            int exifOrientation = getExifOrientation(filePath);
            int angle = getDegreesFromExif(exifOrientation);
            if (angle > -1) {
                Matrix matrix = new Matrix();
                matrix.postRotate(angle);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return bitmap;
    }

    private int getExifOrientation(String filePath) {
        int orientation = -1;
        try {
            ExifInterface exif = new ExifInterface(filePath);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return orientation;
    }

    private void setExifOrientation(int exifOrientation, String srcPath) {

        try {
            ExifInterface exif = new ExifInterface(srcPath);
            exif.setAttribute(ExifInterface.TAG_ORIENTATION, Integer.toString(exifOrientation));
            exif.saveAttributes();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void moveExifOrientation(String srcPath, String destinationPath) {
        int exifOrientation = getExifOrientation(srcPath);
        setExifOrientation(exifOrientation, destinationPath);
    }

    private int getDegreesFromExif(int orientation) {
        int angle = -1;

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
            angle = 90;
        else if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
            angle = 180;
        else if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
            angle = 270;

        return angle;

    }

    public Bitmap loadVideoBitmap(String mVideoToShow) {
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mVideoToShow,
                MediaStore.Images.Thumbnails.MINI_KIND);
        return thumb;
    }
}