package com.kodevincere.piktureeditor.fileutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by alphonselric
 */
public class FileOperations {

    public static boolean copyFile(final File src, final File dst) {
        boolean success = false;

        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            success = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return success;

    }

}
