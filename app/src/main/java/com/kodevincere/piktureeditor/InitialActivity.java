package com.kodevincere.piktureeditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

import com.kodevincere.piktureeditor.base.ui.BaseActivity;
import com.kodevincere.piktureeditor.fileutils.ImageHelper;
import com.kodevincere.piktureeditor.ui.pictureeditor.PiktureEditorActivity;

import butterknife.Bind;
import butterknife.OnClick;

public class InitialActivity extends BaseActivity implements OnTouchListener {

    private final String JPEG_EXTENSION = ".jpg";
    private final String PICTURE_SOURCE = "/storage/emulated/0/bluetooth/Juan".concat(JPEG_EXTENSION);
    private final String PICTURE_DESTINATION = "/storage/emulated/0/testing".concat(JPEG_EXTENSION);

    @Bind(R.id.btn_start_activity) Button btnActivity;
    @Bind(R.id.image_view) ImageView imageView;
    @Bind(R.id.btn_dialog) Button btnDialog;


    @Override
    public int getViewLayout() {
        return R.layout.activity_initial;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bitmap bitmap = new ImageHelper().loadReducedBitmap(PICTURE_SOURCE, 1024, 1024);
        imageView.setImageBitmap(bitmap);

        imageView.setOnTouchListener(this);
    }

    @OnClick(R.id.btn_start_activity)
    void onStartActivityClicked(View v) {
        Intent i = new Intent(this, PiktureEditorActivity.class);
        i.putExtra(PiktureEditorActivity.PICTURE_SOURCE, PICTURE_SOURCE);
        i.putExtra(PiktureEditorActivity.PICTURE_DESTINATION, PICTURE_DESTINATION);
        startActivityForResult(i, PiktureEditorActivity.CODE_EDIT_PICTURE);

    }

    @OnClick(R.id.btn_dialog)
    void onDialogClicked(View v) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            String newPath = data.getExtras().getString(PiktureEditorActivity.PICTURE_DESTINATION);
            Bitmap bitmap = new ImageHelper().loadReducedBitmap(newPath, 1024, 1024);
            imageView.setImageBitmap(bitmap);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        int touchCount = motionEvent.getPointerCount();

        if (touchCount > 1)
            onMultitouch(motionEvent);

        return true;
    }

    private void onMultitouch(MotionEvent event) {
        float firstX = event.getX(0);
        float firstY = event.getY(0);
        float secondX = event.getX(1);
        float secondY = event.getY(1);

        float midX = imageView.getWidth() / 2;
        float midY = imageView.getHeight() / 2;

        float distanceX1 = secondX - firstX;
        float distanceY1 = secondY - firstY;


        btnDialog.setRotation((float) ((Math.atan2(distanceY1, distanceX1) + Math.PI) * 180 / Math.PI));
    }

}
