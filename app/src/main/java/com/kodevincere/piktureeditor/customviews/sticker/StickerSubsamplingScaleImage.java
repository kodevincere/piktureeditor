package com.kodevincere.piktureeditor.customviews.sticker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.kodevincere.piktureeditor.customviews.sticker.StickerLayout.StickerHolderListener;
import com.kodevincere.piktureeditor.model.Size;
import com.kodevincere.piktureeditor.subscaleview.SubsamplingScaleImageView;
import com.kodevincere.piktureeditor.ui._interface.ColorChangeableView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by mE on 8/12/16.
 */

/** Helper over @SubsamplingScaleImageView to allow hosting stickers and all their interactions **/

public class StickerSubsamplingScaleImage extends SubsamplingScaleImageView implements
        StickerHolderListener, ColorChangeableView {

    private Canvas canvas;
    private Path drawnPath = new Path();
    private Paint paint;
    private float lastX, lastY;
    private float downX;
    private float downY;
    private float upY;
    private boolean canWrite = false;

    private List<StickerLayout> stickers = new ArrayList<>(10);
    private List<StickerLayout> texts = new ArrayList<>(10);
    private List<PointF> stickersStartPosition = new ArrayList<>(10);
    private Map<StickerLayout, Size> standardizedSize = new HashMap<>();

    public StickerSubsamplingScaleImage(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public StickerSubsamplingScaleImage(Context context) {
        super(context);
        init();
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        final boolean toReturn = super.onTouchEvent(event);

        final int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (canWrite) {
                    writeDown(event);
                }
                fillStickersStartPositions();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_MOVE:
                if (canWrite) {
                    writeMove(event);
                }
                updateStickerCoordinatesAndSize(event);
                break;
        }

        invalidate();
        return toReturn;
    }

    @Override
    public void onStickerDeleted(StickerLayout stickerLayout) {

        int stickerIndex = stickers.indexOf(stickerLayout);

        if(stickerIndex > -1) {
            stickers.remove(stickerIndex);
            stickersStartPosition.clear();
            standardizedSize.remove(stickerLayout);
            return;
        }

        stickerIndex = texts.indexOf(stickerLayout);

        if(stickerIndex > -1)
            texts.remove(stickerIndex);

    }

    @Override
    protected void onImageLoaded() {
        super.onImageLoaded();
        initCustomCanvas();
    }

    @Override
    public void onColorChanged(int color) {
        setWriteColor(color);
    }

    //Setters

    public void setCanWrite(boolean canWrite) {
        this.canWrite = canWrite;
    }

    public void setWriteColor(int color) {
        paint.setColor(color);
    }

    //Getters

    public Bitmap getMyBitmap() {
        return getBitmap();
    }

    public List<StickerLayout> getStickers() {
        return stickers;
    }

    //Initializers

    private void init() {
        initPaint();
    }

    private void initPaint() {
        paint = new Paint();
        paint.setStrokeWidth(20);

        paint.setDither(true);
        paint.setAntiAlias(true);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    private void initCustomCanvas() {
        Bitmap bitmap = getBitmap();
        canvas = new Canvas(bitmap);
    }

    //Add stickers

    public void addDraggableView(StickerLayout stickerLayout) {
        stickerLayout.setStickerHolderListener(this);
        this.stickers.add(stickerLayout);
    }

    public void addText(StickerSnapchatText stickerSnapchatText) {
        stickerSnapchatText.setStickerHolderListener(this);
        this.texts.add(stickerSnapchatText);
    }

    // Pointer coordinates mapping

    /** Maps view touch coordinates to actual picture coordinates **/
    private final float[] mapPointerCoords(float[] coords){
        final Matrix matrix = new Matrix();
        getImageMatrix().invert(matrix);
        matrix.postTranslate(getScrollX(), getScrollY());
        matrix.mapPoints(coords);
        return coords;
    }

    private final float[] mapPointerCoords(MotionEvent e) {
        final int index = e.getActionIndex();
        final float[] coords = new float[]{e.getX(index), e.getY(index)};
        return mapPointerCoords(coords);
    }

    private final float[] mapPointerCoords(float x, float y) {
        final float[] coords = new float[]{x, y};
        return mapPointerCoords(coords);
    }


    // Write in picture

    private void writeDown(MotionEvent event){
        float[] coords = mapPointerCoords(event);

        downX = coords[0];
        downY = coords[1];
    }

    private void writeMove(MotionEvent event){
        final int historySize = event.getHistorySize();

        for (int i = 0; i < historySize; i++) {

            float[] coords = mapPointerCoords(event.getHistoricalX(i), event.getHistoricalY(i));

            float upX = coords[0];
            upY = coords[1];

            canvas.drawLine(downX, downY, upX, upY, paint);

            downX = upX;
            downY = upY;

        }
    }

    // Draw views in picture

    public Observable<StickerLayout> drawStickers(int sampleSize) {
        final Canvas canvas = new Canvas(getMyBitmap());

        return Observable.from(stickers)
                .subscribeOn(Schedulers.computation())
                .doOnNext(stickerView -> stickerView.drawInCanvas(canvas, this, sampleSize));
    }

    public Observable<StickerLayout> drawText(int sampleSize){
        final Canvas canvas = new Canvas(getMyBitmap());

        return Observable.from(texts)
                .subscribeOn(Schedulers.computation())
                .doOnNext(draggableText -> draggableText.drawInCanvas(canvas, this, sampleSize));
    }

    public Observable<StickerLayout> drawAll(int sampleSize) {
        final Canvas canvas = new Canvas(getMyBitmap());

        int capacity = stickers.size() + texts.size();

        List<StickerLayout> allViews = new ArrayList<>(capacity);
        allViews.addAll(stickers);
        allViews.addAll(texts);

        return Observable.from(allViews)
                .subscribeOn(Schedulers.computation())
                .doOnNext(stickerView -> stickerView.drawInCanvas(canvas, this, sampleSize));
    }

    // Stickers movement

    /** Fills stickers position when starting touch to allow correct movement while dragging picture **/
    private void fillStickersStartPositions() {
        stickersStartPosition = new ArrayList<>(stickers.size());
        for (StickerLayout stickerView : stickers) {
            final ViewGroup.LayoutParams layoutParams = stickerView.getLayoutParams();
            final PointF leftUpperPoint = viewToSourceCoord(stickerView.getX(), stickerView.getY());

            stickersStartPosition.add(leftUpperPoint);

            standardizedSize.put(stickerView, new Size(Math.round((layoutParams.width / scale)),
                    Math.round((layoutParams.height / scale))));
        }
    }

    /** Set correct stickers position and size when dragging and zooming picture**/
    private void updateStickerCoordinatesAndSize(MotionEvent event) {

        int stickerCount = stickers.size();

        for (int i = 0; i < stickerCount ; i++) {
            final StickerLayout sticker = stickers.get(i);
            final PointF initialUpperLeftPoint = stickersStartPosition.get(i);
            final PointF newUpperLeftPoint = sourceToViewCoord(initialUpperLeftPoint);

            // Updates size before setting position to solve inconsistent movements
            if (event.getPointerCount() >= 2)
                updateStickerSize(sticker);

            sticker.updateXPosition(newUpperLeftPoint.x);
            sticker.updateYPosition(newUpperLeftPoint.y);
        }
    }

    /** Scales stickers according to picture zoom **/
    private void updateStickerSize(StickerLayout sticker) {
        Size size = standardizedSize.get(sticker);
        int width = size.getWidth();
        int height = size.getHeight();

        final int newWidth = (int) (width * scale);
        final int newHeight = (int) (height * scale);

        if(newWidth != width && newHeight != height)
            sticker.scale(newWidth, newHeight);
    }

    // List clearers

    public void clearStickers() {
        for (StickerLayout stickerView : stickers) {
            stickerView.delete();
        }
        stickers.clear();
        stickersStartPosition.clear();
        standardizedSize.clear();
    }

    public void clearTexts() {
        for (StickerLayout text : texts) {
            text.delete();
        }
        texts.clear();
    }

    public void clearAll(){
        clearStickers();
        clearTexts();
    }


    //Previous drawers with Path
    private void touchStart(float x, float y) {
        drawnPath.moveTo(x, y);
        lastX = x;
        lastY = y;
    }

    private void touchMove(float x, float y) {
        drawnPath.quadTo(lastX, lastY, (lastX + x) / 2, (lastY + y) / 2);
        lastX = x;
        lastY = y;

        canvas.drawPath(drawnPath, paint);
    }
}