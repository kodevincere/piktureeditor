package com.kodevincere.piktureeditor.customviews.sticker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.kodevincere.piktureeditor.R;
import com.kodevincere.piktureeditor.base.util.KeyboardUtil;
import com.kodevincere.piktureeditor.ui._interface.ColorChangeableView;

/**
 * Created by mE on 9/26/16.
 */

/** Helper class to define editable text in pictures just like Snapchat **/
public class StickerSnapchatText extends StickerLayout implements ColorChangeableView {

    private static final String EMPTY_STRING = "";
    
    private EditText editText;
    private TextView textView;

    private TextEditEventListener textEditEventListener;

    public StickerSnapchatText(Context context) {
        super(context);
        init();
    }

    public StickerSnapchatText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StickerSnapchatText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public boolean canMoveInX() {
        return false;
    }

    @Override
    public boolean canMoveInY() {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int touchCounter = event.getPointerCount();
        if(touchCounter == 1)
            return super.onTouchEvent(event);

        return false;
    }

    @Override
    public void drawInCanvas(Canvas canvas,
                             StickerSubsamplingScaleImage stickerSubsamplingScaleImage,
                             int sampleSize) {
        super.drawInCanvas(canvas, stickerSubsamplingScaleImage, sampleSize);

        final float zoomScale = stickerSubsamplingScaleImage.getScale();

        final String text =  textView.getText().toString();

        final Rect rect = new Rect();

        TextPaint paint = textView.getPaint();
        paint.setTextSize(paint.getTextSize() / (zoomScale * sampleSize));
        paint.getTextBounds(text, 0, text.length(), rect);

        StaticLayout sl = new StaticLayout(text, paint, canvas.getWidth(),
                Layout.Alignment.ALIGN_CENTER, 1, 1, true);

        canvas.save();

        PointF sourceCoordinates =
                stickerSubsamplingScaleImage.viewToSourceCoord(getX(), getY());

        canvas.translate(0, sourceCoordinates.y);

        sl.draw(canvas);

        canvas.restore();

    }

    @Override
    public void onColorChanged(int color) {
        textView.setTextColor(color);
        editText.setTextColor(color);
    }

    //Setters

    public void setEventListener(TextEditEventListener listener) {
        this.textEditEventListener = listener;
    }

    public void setText(String text) {
        if (textView != null)
            textView.setText(text);

        if (editText != null) {
            editText.setText(EMPTY_STRING);
            editText.append(text);
        }
    }

    //Getters

    public String getText() {
        return editText.getText().toString();
    }

    //Initializers

    private void init(){
        initDimensions();
        initViews();
    }

    private void initDimensions(){
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);

        setLayoutParams(layoutParams);

        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.black_overlay));

        postInvalidate();
        requestLayout();
    }

    private void initViews(){
        initEditText();
        initTextView();
        addViews();
    }

    private void initEditText(){

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        editText = new EditText(getContext());
        editText.setLayoutParams(params);

        editText.setSingleLine();
        editText.setBackground(null);
        editText.setVisibility(GONE);
        editText.setTextColor(Color.YELLOW);
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        editText.onEditorAction(EditorInfo.IME_ACTION_DONE);

        editText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (EditorInfo.IME_ACTION_DONE == actionId)
                finishEditingText();

            return false;
        });

        editText.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus)
                finishEditingText();
        });
    }

    private void initTextView(){
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        textView = new TextView(getContext());

        textView.setLayoutParams(params);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.YELLOW);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
    }

    private void addViews(){
        addView(editText);
        addView(textView);
    }

    // Edition Interactions

    public void startEditingText() {
        textView.setVisibility(GONE);
        editText.setVisibility(VISIBLE);

        editText.requestFocus();
        KeyboardUtil.show(editText);

        if (textEditEventListener != null)
            textEditEventListener.onTextEditionStarted(this);
    }

    private void finishEditingText() {
        setText(editText.getText().toString());
        showText();

        if (textEditEventListener != null)
            textEditEventListener.onTextEditionFinished(this);
    }

    public void showText() {
        textView.setVisibility(VISIBLE);
        editText.setVisibility(GONE);

        editText.clearFocus();
        KeyboardUtil.hide(editText);
    }

    /** Interface to notify interested entities about text changes **/

    public interface TextEditEventListener {
        void onTextEditionStarted(StickerSnapchatText stickerSnapchatText);

        void onTextEditionFinished(StickerSnapchatText stickerSnapchatText);
    }

}
