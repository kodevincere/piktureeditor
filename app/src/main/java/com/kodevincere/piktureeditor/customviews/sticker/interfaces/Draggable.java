package com.kodevincere.piktureeditor.customviews.sticker.interfaces;

import android.view.MotionEvent;

/**
 * Created by mE on 9/29/16.
 */
public interface Draggable {
    void updatePosition(float newX, float newY);
    void updatePosition(float newX, float newY, MotionEvent motionEvent);
    void updateXPosition(float xOffset);
    void updateYPosition(float yOffset);
}
