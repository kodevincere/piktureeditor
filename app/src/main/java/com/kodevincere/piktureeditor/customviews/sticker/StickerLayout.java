package com.kodevincere.piktureeditor.customviews.sticker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.kodevincere.piktureeditor.customviews.sticker.interfaces.Deletable;
import com.kodevincere.piktureeditor.customviews.sticker.interfaces.Draggable;
import com.kodevincere.piktureeditor.customviews.sticker.interfaces.Rotatable;
import com.kodevincere.piktureeditor.customviews.sticker.interfaces.Scalable;
import com.kodevincere.piktureeditor.customviews.sticker.interfaces.SelfDrawable;

/**
 * Created by mE on 9/29/16.
 * Layout that allows to click, drag, resize, rotate, delete, etc
 */
public abstract class StickerLayout extends FrameLayout implements Draggable, Scalable, Rotatable,
        Deletable, SelfDrawable {

    protected final static double PI_UNDER_180 = 180 / Math.PI;
    protected int minPixels; // Min pixels size for this view

    private PointF initialPosition = new PointF(); // Position when first touched with one finger
    private PointF initialMovePosition = new PointF(); // First touch when moving with one finger
    private PointF initialSize = new PointF(); // Initial size when touched with more than one finger
    private float initialAngle = 0; // Initial view angle when starting rotation
    private float initialFingerAngle = 0; // Initial finger angle when starting rotation
    private float fingerDistanceStart; // Distance of two fingers when first touched with them
    private float fixX; // x axis difference in rawX and getX
    private float fixY; // y axis difference in rawY and getY (Mostly because actionbar)
    protected float actualRotation = 0; // Actual view rotation
    private boolean canPerformClick = true; // Check if click is allowed, because there were some issues with multitouch and click
    private boolean canPerformClickWasUpdated = false; // Allows only one change in canPerformClick while moving
    private ViewGroup.LayoutParams mLayoutParams; // My layout params

    private StickerHolderListener stickerHolderListener;
    private StickerDragListener stickerDragListener;

    public StickerLayout(Context context) {
        super(context);
    }

    public StickerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StickerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public abstract boolean canMoveInX();

    public abstract boolean canMoveInY();

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(mLayoutParams == null)
            mLayoutParams = getLayoutParams();


        final int touch = event.getPointerCount();

        if (touch == 1) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    fixX = event.getRawX() - (getX() + event.getX(0));
                    fixY = event.getRawY() - (getY() + event.getY(0));

                    loadActualMovementState(event, 0);

                    break;
                case MotionEvent.ACTION_MOVE:

                    float xOffset = event.getRawX() - initialMovePosition.x;
                    float yOffset = event.getRawY() - initialMovePosition.y;

                    updatePosition(initialPosition.x + xOffset, initialPosition.y + yOffset, event);

                    //Some devices call action_move when finger pressed on the screen
                    //I'm looking at you, Lenovo tablet.
                    boolean hasMoved = Math.abs(xOffset) > 0 && Math.abs(yOffset) > 0;

                    if (!canPerformClickWasUpdated && hasMoved) {
                        canPerformClick = !canPerformClick;
                        canPerformClickWasUpdated = true;
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    xOffset = initialMovePosition.x - event.getRawX();
                    yOffset = initialMovePosition.y - event.getRawY();

                    hasMoved = Math.abs(xOffset) > 5 && Math.abs(yOffset) > 5;

                    if(stickerDragListener!= null)
                        stickerDragListener.onStickerIsReleased(this, event);

                    if (canPerformClick && !hasMoved)
                        performClick();

                    canPerformClick = true;
                    canPerformClickWasUpdated = false;

                    return false;
            }
        } else
            multitouch(event);

        return true;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.save();
        canvas.rotate(actualRotation, this.getWidth() / 2, this.getHeight() / 2);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    @Override
    public void updatePosition(float newX, float newY) {
        updateXPosition(newX);
        updateYPosition(newY);
    }

    @Override
    public void updatePosition(float newX, float newY, MotionEvent motionEvent) {
        updatePosition(newX, newY);

        if(canMoveInX() || canMoveInY()){
            if(stickerDragListener != null)
                stickerDragListener.onStickerIsBeingDragged(this, motionEvent);
        }
    }

    @Override
    public void updateXPosition(float newX) {
        if(canMoveInX())
            setX(newX);
    }

    @Override
    public void updateYPosition(float newY) {
        if(canMoveInY())
            setY(newY);
    }

    @Override
    public void scale(int newWidth, int newHeight) {
        final boolean scalingUpWidth = newWidth > getWidth();
        final boolean scalingUpHeight = newHeight > getHeight();
        final boolean isScalingUp = scalingUpWidth && scalingUpHeight;

        onScaling(isScalingUp);
        mLayoutParams.width = newWidth;
        mLayoutParams.height = newHeight;

        postInvalidate();
        requestLayout();
    }

    @Override
    public void rotate(MotionEvent event) {
        actualRotation = initialAngle + ((float) calculateFingersAngle(event) - initialFingerAngle);
        onRotating();
        invalidate();
    }

    @Override
    public void delete() {
        ViewGroup parent = ((ViewGroup) getParent());

        if (parent != null)
            parent.removeView(this);

        if(stickerHolderListener != null)
            stickerHolderListener.onStickerDeleted(this);
    }

    @Override
    public void drawInCanvas(Canvas canvas,
                             StickerSubsamplingScaleImage stickerSubsamplingScaleImage,
                             int sampleSize) {

    }

    //Setters

    public void setStickerHolderListener(StickerHolderListener stickerHolderListener) {
        this.stickerHolderListener = stickerHolderListener;
    }

    public void setStickerDragListener(StickerDragListener stickerDragListener) {
        this.stickerDragListener = stickerDragListener;
    }

    //Getters

    public float getActualRotation() {
        return actualRotation;
    }

    //Touch events

    private void multitouch(MotionEvent event) {

        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_1_DOWN:
            case MotionEvent.ACTION_POINTER_2_DOWN:
                initialSize.set(mLayoutParams.width, mLayoutParams.height);

                fingerDistanceStart = distance(event.getX(0), event.getX(1),
                        event.getY(0), event.getY(1));
                centerInFingers(event);

                initialAngle = actualRotation;
                initialFingerAngle = (float) calculateFingersAngle(event);

                break;
            case MotionEvent.ACTION_MOVE:

                final float fingerDistanceEnd = distance(event.getX(0), event.getX(1),
                        event.getY(0), event.getY(1));

                final float scale = (fingerDistanceEnd / fingerDistanceStart);

                final int newWidth = (int) (initialSize.x * scale);
                final int newHeight = (int) (initialSize.y * scale);

                final View parent = ((View) getParent());

                final boolean widthSmallerThanParent = newWidth < parent.getWidth();
                final boolean heightSmallerThanParent = newHeight < parent.getHeight();
                final boolean sizeSmallerThanParent = widthSmallerThanParent && heightSmallerThanParent;

                final boolean widthLargerThanMin = newWidth > minPixels;
                final boolean heightLargerThanMin = newHeight > minPixels;
                final boolean sizeLargerThanMin = widthLargerThanMin && heightLargerThanMin;

                rotate(event);

                centerInFingers(event);

                if (sizeSmallerThanParent && sizeLargerThanMin)
                    scale(newWidth, newHeight);

                break;

            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_POINTER_2_UP:

                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK)
                        >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;

                loadActualMovementState(event, pointerIndex == 0 ? 1 : 0);

                if(stickerDragListener!= null)
                    stickerDragListener.onStickerIsReleased(this, event);

                canPerformClick = false;

                break;
        }
    }

    //Movements

    private void loadActualMovementState(MotionEvent event, int pointerIndex) {

        final float x = getX();
        final float y = getY();

        initialPosition.set(x, y);
        initialMovePosition.set(x + event.getX(pointerIndex) + fixX,
                y + event.getY(pointerIndex) + fixY);

        initialAngle = actualRotation;

    }

    private void centerInFingers(MotionEvent event) {
        final float centerX = getX() + mLayoutParams.width / 2;
        final float centerY = getY() + mLayoutParams.height / 2;

        final float eventCenterX = getX() + (event.getX(0) + event.getX(1)) / 2;
        final float eventCenterY = getY() + (event.getY(0) + event.getY(1)) / 2;

        updatePosition(getX() + eventCenterX - centerX, getY() + eventCenterY - centerY);

    }

    //Calculations

    private float distance(float x0, float x1, float y0, float y1) {
        final float x = x0 - x1;
        final float y = y0 - y1;
        return (float) Math.sqrt(x * x + y * y);
    }

    protected int convertDpToPixel(float dp, Context context) {
        final Resources resources = context.getResources();
        final DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

    private double calculateFingersAngle(MotionEvent event) {
        final float firstX = event.getX(0);
        final float firstY = event.getY(0);
        final float secondX = event.getX(1);
        final float secondY = event.getY(1);

        final float distanceX = secondX - firstX;
        final float distanceY = secondY - firstY;

        return (Math.atan2(distanceY, distanceX) + Math.PI) * PI_UNDER_180;

    }

    // Layout changes

    protected void onScaling(boolean scaleUp) {
    }

    protected void onRotating() {
    }

    public interface StickerHolderListener {
        void onStickerDeleted(StickerLayout stickerLayout);
    }

    public interface StickerDragListener{
        void onStickerIsBeingDragged(StickerLayout stickerLayout, MotionEvent motionEvent);
        void onStickerIsReleased(StickerLayout stickerLayout, MotionEvent motionEvent);
    }

}