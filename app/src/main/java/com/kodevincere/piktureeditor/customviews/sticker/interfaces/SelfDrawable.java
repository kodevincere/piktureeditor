package com.kodevincere.piktureeditor.customviews.sticker.interfaces;

import android.graphics.Canvas;

import com.kodevincere.piktureeditor.customviews.sticker.StickerSubsamplingScaleImage;

/**
 * Created by mE on 9/29/16.
 */
public interface SelfDrawable {
    void drawInCanvas(Canvas canvas,
                      StickerSubsamplingScaleImage stickerSubsamplingScaleImage,
                      int sampleSize);
}
