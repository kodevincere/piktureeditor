package com.kodevincere.piktureeditor.customviews.sticker.interfaces;

/**
 * Created by mE on 9/29/16.
 */
public interface Deletable {
    void delete();
}
