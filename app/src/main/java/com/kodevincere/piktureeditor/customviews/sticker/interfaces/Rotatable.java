package com.kodevincere.piktureeditor.customviews.sticker.interfaces;

import android.view.MotionEvent;

/**
 * Created by mE on 9/29/16.
 */
public interface Rotatable {
    void rotate(MotionEvent motionEvent);
}
