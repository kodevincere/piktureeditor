package com.kodevincere.piktureeditor.customviews.sticker;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.kodevincere.piktureeditor.base.util.KeyboardUtil;
import com.kodevincere.piktureeditor.customviews.util.AutoResizeTextView;
import com.kodevincere.piktureeditor.ui._interface.ColorChangeableView;


/**
 * Created by cheungchingai on 6/15/15.
 * modified by mE 2016
 *
 * Sticker that holds text
 *
 */
public class StickerTextView extends StickerView implements ColorChangeableView {

    private final static float SUPER_SCIENTIFICALLY_CALCULATED_CORRECTION_FACTOR = 0.3f;

    private AutoResizeTextView tvMain;
    private EditText etMain;
    private TextEventListener listener;

    // Correction factor for locating stickers
    private float horizontalCorrectionFactor = 1.03f;
    private float verticalCorrectionFactor = 1.04f;


    public StickerTextView(Context context) {
        super(context);
    }

    public StickerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StickerTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public View getMainView() {
        createViews();
        return tvMain;
    }

    @Override
    protected View getSecondaryView() {
        createViews();
        return etMain;
    }

    @Override
    public boolean canMoveInX() {
        return true;
    }

    @Override
    public boolean canMoveInY() {
        return true;
    }

    @Override
    public void drawInCanvas(Canvas canvas,
                             StickerSubsamplingScaleImage stickerSubsamplingScaleImage,
                             int sampleSize) {
        super.drawInCanvas(canvas, stickerSubsamplingScaleImage, sampleSize);

        final float zoomScale = stickerSubsamplingScaleImage.getScale();

        final TextView textView = this.getTextView();

        final PointF pointF = stickerSubsamplingScaleImage
                .viewToSourceCoord(this.getX() + textView.getWidth() / 2,
                        this.getY() + textView.getHeight() / 2);

        final String text = this.getText();

        final Rect rect = new Rect();
        final Paint paint = textView.getPaint();
        paint.setTextSize(paint.getTextSize() / (zoomScale * sampleSize));
        paint.getTextBounds(text, 0, text.length(), rect);

        canvas.save();
        canvas.rotate(this.getActualRotation(),
                pointF.x / sampleSize, pointF.y / sampleSize);

        canvas.drawText(this.getText(),
                pointF.x / sampleSize - (rect.width() * horizontalCorrectionFactor) / 2,
                pointF.y / sampleSize + (rect.height() * verticalCorrectionFactor) / 2, paint);

        canvas.restore();

    }

    @Override
    public void onColorChanged(int color) {
        tvMain.setTextColor(color);
        etMain.setTextColor(color);
    }

    //Setters

    public void setText(String text) {
        if (tvMain != null)
            tvMain.setText(text);

        if (etMain != null) {
            etMain.setText("");
            etMain.append(text);
        }
    }

    public void setEventListener(TextEventListener listener) {
        this.listener = listener;
    }

    //Getters

    public String getText() {
        if (tvMain != null)
            return tvMain.getText().toString();

        return null;
    }

    public TextView getTextView() {
        return tvMain;
    }

    //Initializers

    private void createViews() {
        if (tvMain == null)
            createTextView();

        if (etMain == null)
            createEditText();
    }

    private void createTextView() {
        tvMain = new AutoResizeTextView(getContext());
        tvMain.setTextColor(Color.BLACK);

        tvMain.setGravity(Gravity.CENTER);
        tvMain.setTextSize(800);
        tvMain.setMaxLines(1);
    }

    private void createEditText() {
        etMain = new EditText(getContext());
        etMain.setTextColor(Color.BLACK);
        etMain.setSingleLine();
        etMain.onEditorAction(EditorInfo.IME_ACTION_DONE);
        etMain.setVisibility(INVISIBLE);

        etMain.setBackground(null);
        etMain.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (EditorInfo.IME_ACTION_DONE == actionId)
                finishEditingText();

            return false;
        });

        etMain.setOnFocusChangeListener((view, hasFocus) -> {
            if (!hasFocus)
                finishEditingText();
        });
    }

    // Edition interaction

    public void startEditingText() {
        tvMain.setVisibility(INVISIBLE);
        etMain.setVisibility(VISIBLE);

        etMain.setTextSize((tvMain.getTextSize() *
                SUPER_SCIENTIFICALLY_CALCULATED_CORRECTION_FACTOR));
        etMain.requestFocus();
        KeyboardUtil.show(etMain);

        if (listener != null)
            listener.onTextEditionStarted(this);
    }

    private void finishEditingText() {
        setText(etMain.getText().toString());
        showText();

        if (listener != null)
            listener.onTextEditionFinished(this);
    }

    public void showText() {
        tvMain.setVisibility(VISIBLE);
        etMain.setVisibility(INVISIBLE);

        etMain.clearFocus();
        KeyboardUtil.hide(etMain);
    }

    /** Interface to notify interested entities about text changes **/
    public interface TextEventListener {
        void onTextEditionStarted(StickerTextView self);

        void onTextEditionFinished(StickerTextView self);
    }
}
