package com.kodevincere.piktureeditor.customviews.sticker;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/** Helper class to define common behaviour regarding some general kind of stickers **/
public abstract class StickerView extends StickerLayout {

    private final static int SELF_SIZE_DP = 100;

    public StickerView(Context context) {
        super(context);
        init();
    }

    public StickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    protected abstract View getMainView();

    protected abstract View getSecondaryView();

    //Initializers

    private void init() {

        minPixels = convertDpToPixel(64, getContext());

        int width = convertDpToPixel(SELF_SIZE_DP, getContext());
        int height = width;

        View mainView = getMainView();
        View secondaryView = getSecondaryView();

        LayoutParams this_params = new LayoutParams(width, height);

        this_params.gravity = Gravity.CENTER;

        LayoutParams iv_main_params =
                new LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );

        iv_main_params.setMargins(0, 0, 0, 0);

        this.setLayoutParams(this_params);
        this.addView(mainView, iv_main_params);

        if (secondaryView != null)
            this.addView(secondaryView, iv_main_params);
    }

    /** Helper class to draw border around stickers **/
    private class BorderView extends View {

        private Paint borderPaint = new Paint();

        public BorderView(Context context) {
            super(context);
            initPaint();
        }

        public BorderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            initPaint();
        }

        public BorderView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            initPaint();
        }

        private void initPaint() {
            borderPaint.setStrokeWidth(6);
            borderPaint.setColor(Color.WHITE);
            borderPaint.setStyle(Paint.Style.STROKE);
            borderPaint.setAntiAlias(true);
            borderPaint.setDither(true);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) this.getLayoutParams();

            Rect border = new Rect();
            border.left = this.getLeft() - params.leftMargin;
            border.top = this.getTop() - params.topMargin;
            border.right = this.getRight() - params.rightMargin;
            border.bottom = this.getBottom() - params.bottomMargin;

            canvas.drawRect(border, borderPaint);

        }
    }

}
