package com.kodevincere.piktureeditor.customviews.sticker;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.kodevincere.piktureeditor.model.Size;

/** Sticker that holds a picture **/
public class StickerImageView extends StickerView {

    private ImageView ivMain;
    private int resourceId;

    public StickerImageView(Context context) {
        super(context);
    }

    public StickerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StickerImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public View getMainView() {
        if (this.ivMain == null) {
            this.ivMain = new ImageView(getContext());
            this.ivMain.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        return ivMain;
    }

    @Override
    protected View getSecondaryView() {
        return null;
    }

    @Override
    public boolean canMoveInX() {
        return true;
    }

    @Override
    public boolean canMoveInY() {
        return true;
    }

    @Override
    public void drawInCanvas(Canvas canvas,
                             StickerSubsamplingScaleImage stickerSubsamplingScaleImage,
                             int sampleSize) {
        super.drawInCanvas(canvas, stickerSubsamplingScaleImage, sampleSize);

        final float zoomScale = stickerSubsamplingScaleImage.getScale();

        final Point imageViewCenter = this.getImageViewCenterPosition();
        final PointF pointF = stickerSubsamplingScaleImage
                .viewToSourceCoord(imageViewCenter.x, imageViewCenter.y);

        final Bitmap stickerBitmap = this.getImageBitmap();

        final PointF ratios = this.calcRatios(zoomScale);

        final Matrix matrix = new Matrix();

        matrix.reset();
        matrix.postTranslate((-stickerBitmap.getWidth()) / 2, (-stickerBitmap.getHeight()) / 2);
        matrix.postRotate(this.getActualRotation());
        matrix.postScale(ratios.x / sampleSize, ratios.y / sampleSize);
        matrix.postTranslate(pointF.x / sampleSize, pointF.y / sampleSize);

        canvas.drawBitmap(stickerBitmap, matrix, null);

    }

    //Setters

    public void setImageBitmap(Bitmap bmp) {
        this.ivMain.setImageBitmap(bmp);
    }

    public void setImageResource(int res_id) {
        this.resourceId = res_id;
        this.ivMain.setImageResource(this.resourceId);
    }

    public void setImageDrawable(Drawable drawable) {
        this.ivMain.setImageDrawable(drawable);
    }

    //Getters

    public Bitmap getImageBitmap() {
        return ((BitmapDrawable) this.ivMain.getDrawable()).getBitmap();
    }

    public Size getImageViewSize() {
        return new Size(ivMain.getWidth(), ivMain.getHeight());
    }

    public Point getImageViewCenterPosition() {
        return new Point((int) (getX()) + getWidth() / 2, (int) (getY()) + getHeight() / 2);
    }

    //Calculators

    public PointF calcRatios(float zoomScale) {
        final Size imageViewSize = getImageViewSize();
        final Bitmap bitmapOverlay = getImageBitmap();

        float ratioWidth = (imageViewSize.getWidth() / (bitmapOverlay.getWidth() * zoomScale));
        float ratioHeight = (imageViewSize.getHeight() / (bitmapOverlay.getHeight() * zoomScale));
        return new PointF(ratioWidth, ratioHeight);
    }
}
