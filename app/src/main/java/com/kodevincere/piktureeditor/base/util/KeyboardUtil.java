package com.kodevincere.piktureeditor.base.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.kodevincere.piktureeditor.base.ui.BaseDialogFragment;
import com.kodevincere.piktureeditor.base.ui.BaseFragment;

/**
 * Created by James
 */
public final class KeyboardUtil {

    public static void hide(Activity activity) {
        if (activity == null) return;

        hide(activity.getCurrentFocus());
    }

    public static void hide(BaseFragment fragment) {
        if (fragment == null || fragment.getActivity() == null)
            return;

        hide(fragment.getActivity());
    }

    public static void hide(BaseDialogFragment fragment) {
        if (fragment == null || fragment.getActivity() == null)
            return;

        hide(fragment.getActivity());
    }


    public static void hide(View view) {
        if (view == null) return;

        view.clearFocus();
        try {
            getInputMethodManager(view).hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static InputMethodManager getInputMethodManager(View view) {
        return (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public static void show(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
}
