package com.kodevincere.piktureeditor.base.recycleview;

/**
 * Created by James
 */
public interface OnActionAdapter {
    void onActionAdapter(ActionPack actionPack);
}
