package com.kodevincere.piktureeditor.base.launcher;


import com.kodevincere.piktureeditor.base.ui.BaseFragment;

/**
 * Created by James
 */
public class FragmentLauncher {

    private BaseFragment fragment;
    private boolean add;
    private boolean time;
    private boolean backstack;
    private boolean delete;
    private boolean animation;
    private boolean onlyDistinct;

    public static FragmentLauncher make() {
        return new FragmentLauncher();
    }

    private FragmentLauncher() {
        add = false;
        time = false;
        backstack = false;
        delete = false;
        animation = false;
        onlyDistinct = false;
    }

    public FragmentLauncher fragment(BaseFragment fragment) {
        this.fragment = fragment;
        return this;
    }

    public FragmentLauncher add() {
        this.add = true;
        return this;
    }

    public FragmentLauncher replace() {
        this.add = false;
        return this;
    }

    public FragmentLauncher time() {
        this.time = true;
        return this;
    }

    public FragmentLauncher backstack() {
        this.backstack = true;
        return this;
    }

    public FragmentLauncher delete() {
        this.delete = true;
        return this;
    }

    public FragmentLauncher animation() {
        this.animation = true;
        return this;
    }

    public FragmentLauncher onlyDistinct() {
        this.onlyDistinct = true;
        return this;
    }

    public BaseFragment getFragment() {
        return fragment;
    }

    public boolean isAdd() {
        return add;
    }

    public boolean isTime() {
        return time;
    }

    public boolean isBackstack() {
        return backstack;
    }

    public boolean isDelete() {
        return delete;
    }

    public boolean isAnimation() {
        return animation;
    }

    public boolean isOnlyDistinct() {
        return onlyDistinct;
    }
}
