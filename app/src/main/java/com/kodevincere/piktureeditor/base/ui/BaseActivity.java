package com.kodevincere.piktureeditor.base.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.kodevincere.piktureeditor.base.launcher.ActivityLauncher;
import com.kodevincere.piktureeditor.base.launcher.FragmentLauncher;
import com.kodevincere.piktureeditor.base.launcher.LauncherViews;
import com.kodevincere.piktureeditor.base.mvp.BasePresenter;
import com.kodevincere.piktureeditor.base.util.KeyboardUtil;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements OnManageNavigation, BaseViewModel {

    private BaseResources baseResources;
    @Inject
    public P presenter;

    protected FragmentManager fragmentManager;
    protected BaseFragment activeFragment;
    protected int rootLayout;

    private LauncherViews activeLauncherViews = LauncherViews.NONE;
    private boolean distinctViewChange = true;
    private Map<LauncherViews, Bundle> hashMapExtras;
    protected ActivityLauncher activityLauncherIntent;

    public abstract int getViewLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getViewLayout());
        ButterKnife.bind(this);
        startPresenter();
        if (presenter != null)
            presenter.create();

        baseResources = getBaseResources();
        if (baseResources != null) ButterKnife.bind(baseResources, this);

        fragmentManager = getSupportFragmentManager();
        rootLayout = getRootLayout();
        hashMapExtras = new HashMap<>();

        processIntent(getIntent());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (presenter != null)
            presenter.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.resume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (presenter != null)
            presenter.activityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null)
            presenter.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (presenter != null)
            presenter.destroy();

        ButterKnife.unbind(this);
        if (baseResources != null)
            ButterKnife.unbind(baseResources);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    onUpNavigationPressed();
                    fragmentManager.popBackStack();

                    if (fragmentManager.getBackStackEntryCount() == 1)
                        onFixedFragmentCall();
                } else {
                    Intent upIntent = NavUtils.getParentActivityIntent(this);
                    if (upIntent != null) {
                        onUpNavigationPressed();

                        upIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //| Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(upIntent);
                        finish();
                    } else {
                        onBackPressed();
                    }
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }

        if (fragmentManager.getBackStackEntryCount() == 1)
            onFixedFragmentCall();
    }

    public void startPresenter() {
    }

    public int getRootLayout() {
        return 0;
    }

    public BaseResources getBaseResources() {
        return BaseResources.nill;
    }

    @Override
    public final Context getViewContext() {
        return this;
    }

    @Override
    public void onShowView(LauncherViews launcherViews) {
        distinctViewChange = (activeLauncherViews != launcherViews);
        activeLauncherViews = launcherViews;
    }

    @Override
    public final void onSaveExtra(LauncherViews launcherViews, Bundle bundle) {
        hashMapExtras.put(launcherViews, bundle);
    }

    @Override
    public void onChangeTitle(String title) {

    }

    @Override
    public void setResultCode(LauncherViews launcherViews, int resultCode) {
    }

    @Override
    public final void popBackStack() {
        onBackPressed();
    }

    @Override
    public void onFinish() {
        finish();
    }

    @Override
    public final Bundle bundleForMe(LauncherViews launcherViews) {
        if (hashMapExtras.containsKey(launcherViews))
            return hashMapExtras.remove(launcherViews);

        return null;
    }

    public void onUpNavigationPressed() {
    }

    public void onFixedFragmentCall() {
    }

    public final void hideKeyboard() {
        KeyboardUtil.hide(this);
    }

    public final void changeFragment(FragmentLauncher fragmentLauncher) {
        boolean continues = true;

        if (fragmentLauncher.isOnlyDistinct())
            continues = distinctViewChange;

        if (continues) {
            try {
                if (fragmentLauncher.isDelete() && activeFragment != null)
                    fragmentManager.beginTransaction().remove(activeFragment).commit();

                activeFragment = fragmentLauncher.getFragment();

                final FragmentTransaction ft = fragmentManager.beginTransaction();

                if (fragmentLauncher.isAnimation())
                    ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);

                if (fragmentLauncher.isBackstack())
                    ft.addToBackStack(null);

                if (fragmentLauncher.isAdd())
                    ft.add(rootLayout, activeFragment, activeLauncherViews.toString());
                else
                    ft.replace(rootLayout, activeFragment, activeLauncherViews.toString());

                if (fragmentLauncher.isTime()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ft.commit();
                        }
                    }, 400);
                } else {
                    ft.commit();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void processIntent(Intent intent) {
        if (intent != null) {
            activityLauncherIntent = ActivityLauncher.transform(intent);
        }
    }

    public final Bundle getBundleLauncher(LauncherViews launcherViews) {
        if (hashMapExtras.containsKey(launcherViews))
            return hashMapExtras.remove(launcherViews);

        return null;
    }
}