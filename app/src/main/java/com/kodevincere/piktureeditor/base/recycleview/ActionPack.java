package com.kodevincere.piktureeditor.base.recycleview;

/**
 * Created by James
 */
public class ActionPack {

    private int idView;
    private int viewType;
    private int position;
    private Object item;
    private Action action;
    private ActionOptions option;

    public static ActionPack make(int idView) {
        return new ActionPack(idView);
    }

    private ActionPack(int idView) {
        this.idView = idView;
    }

    public ActionPack item(Object item) {
        this.item = item;
        return this;
    }

    public ActionPack viewType(int viewType) {
        this.viewType = viewType;
        return this;
    }

    public ActionPack position(int position) {
        this.position = position;
        return this;
    }

    public ActionPack action(Action action) {
        this.action = action;
        return this;
    }

    public ActionPack option(ActionOptions option) {
        this.option = option;
        return this;
    }

    public int getIdView() {
        return idView;
    }

    public int getViewType() {
        return viewType;
    }

    public int getPosition() {
        return position;
    }

    public Object getItem() {
        return item;
    }

    public ActionOptions getOption() {
        return option;
    }

    public Action getAction() {
        return action;
    }

    public enum Action {
        CLICK,
        LONGCLICK,
        TOUCH,
        OTHER,
    }

    public enum OptionEnum implements ActionOptions {
        NONE
    }

    @Override
    public String toString() {
        return "ActionPack {" +
                ", idView=" + idView +
                ", viewType=" + viewType +
                ", position=" + position +
                ", item=" + item +
                ", action=" + action +
                ", option=" + option +
                '}';
    }
}
