package com.kodevincere.piktureeditor.base.ui;

import android.content.Context;

public interface BaseViewModel {
    Context getViewContext();
}
