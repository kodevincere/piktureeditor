package com.kodevincere.piktureeditor.base.launcher;

import com.kodevincere.piktureeditor.InitialActivity;

/**
 * Created by James
 */
public enum LauncherViews {
    NONE,
    InitialActivity(InitialActivity.class);


    private Class cls;

    LauncherViews() {
    }

    LauncherViews(Class cls) {
        this.cls = cls;
    }

    public Class getCls() {
        return cls;
    }
}
