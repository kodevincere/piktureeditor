package com.kodevincere.piktureeditor.base.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by James
 */
public class ActivityLauncher {

    private static final String EXTRA_BUNDLE = "extra_view_router";
    private static final String EXTRA_ACTIVITY = "extra_activity";
    private static final String EXTRA_FRAGMENT = "extra_fragment";
    private static final String EXTRA_BUNDLE_ACTIVITY = "extra_bundle_activity";
    private static final String EXTRA_BUNDLE_FRAGMENT = "extra_bundle_fragment";
    private static final String EXTRA_ONLY_ACTIVITY = "extra_only_activity";
    private static final String EXTRA_FOR_RESULT = "extra_for_result";
    private static final String EXTRA_CODE_RESULT = "extra_code_result";

    private LauncherViews activity;
    private LauncherViews fragment;
    private Bundle extrasActivity;
    private Bundle extrasFragment;
    private boolean onlyActivity;
    private boolean forResult = false;
    private int codeResult = Activity.RESULT_OK;

    public static ActivityLauncher make() {
        return new ActivityLauncher();
    }

    private ActivityLauncher() {
    }

    public ActivityLauncher startActivity(LauncherViews activity) {
        this.activity = activity;
        return this;
    }

    public ActivityLauncher extrasActivity(Bundle extrasActivity) {
        if (extrasActivity != null)
            this.extrasActivity = extrasActivity;
        return this;
    }

    public ActivityLauncher goFragment(LauncherViews fragment) {
        if (fragment != LauncherViews.NONE)
            this.fragment = fragment;

        return this;
    }

    public ActivityLauncher extrasFragment(Bundle extrasFragment) {
        if (extrasFragment != null)
            this.extrasFragment = extrasFragment;
        return this;
    }

    public ActivityLauncher forResult(int codeResult) {
        this.forResult = true;
        this.codeResult = codeResult;
        return this;
    }

    public void launch(Activity activity) {
        if (isForResult()) {
            activity.startActivityForResult(makeIntent(activity), getCodeResult());
        } else {
            activity.startActivity(makeIntent(activity));
        }
    }

    public void launch(Context context) {
        if (!isForResult())
            context.startActivity(makeIntent(context));
    }

    public Intent makeIntent(Context context) {
        Intent intent = new Intent(context, activity.getCls());
        Bundle bundle = new Bundle();

        if (fragment == null) onlyActivity = true;
        else {
            onlyActivity = false;
            bundle.putSerializable(EXTRA_FRAGMENT, fragment);
        }

        bundle.putSerializable(EXTRA_ACTIVITY, activity);

        intent.putExtra(EXTRA_BUNDLE, bundle);
        intent.putExtra(EXTRA_ONLY_ACTIVITY, onlyActivity);
        intent.putExtra(EXTRA_FOR_RESULT, forResult);
        intent.putExtra(EXTRA_CODE_RESULT, codeResult);

        if (extrasActivity != null)
            intent.putExtra(EXTRA_BUNDLE_ACTIVITY, extrasActivity);

        if (extrasFragment != null)
            intent.putExtra(EXTRA_BUNDLE_FRAGMENT, extrasFragment);

        return intent;
    }

    public static ActivityLauncher transform(Intent intent) {
        ActivityLauncher r = new ActivityLauncher();

        if (intent.hasExtra(EXTRA_BUNDLE)) {
            Bundle bundle = intent.getBundleExtra(EXTRA_BUNDLE);

            if (bundle.containsKey(EXTRA_ACTIVITY))
                r.activity = (LauncherViews) bundle.getSerializable(EXTRA_ACTIVITY);

            if (bundle.containsKey(EXTRA_FRAGMENT))
                r.fragment = (LauncherViews) bundle.getSerializable(EXTRA_FRAGMENT);
        }

        if (intent.hasExtra(EXTRA_BUNDLE_ACTIVITY))
            r.extrasActivity = intent.getBundleExtra(EXTRA_BUNDLE_ACTIVITY);

        if (intent.hasExtra(EXTRA_BUNDLE_FRAGMENT))
            r.extrasFragment = intent.getBundleExtra(EXTRA_BUNDLE_FRAGMENT);

        if (intent.hasExtra(EXTRA_ONLY_ACTIVITY))
            r.onlyActivity = intent.getBooleanExtra(EXTRA_ONLY_ACTIVITY, false);

        if (intent.hasExtra(EXTRA_FOR_RESULT))
            r.forResult = intent.getBooleanExtra(EXTRA_FOR_RESULT, false);

        if (intent.hasExtra(EXTRA_CODE_RESULT))
            r.codeResult = intent.getIntExtra(EXTRA_CODE_RESULT, Activity.RESULT_OK);

        return r;
    }

    public LauncherViews getActivity() {
        return activity;
    }

    public LauncherViews getFragment() {
        return fragment;
    }

    public Bundle getExtrasActivity() {
        return extrasActivity;
    }

    public Bundle getExtrasFragment() {
        return extrasFragment;
    }

    public boolean isOnlyActivity() {
        return onlyActivity;
    }

    public boolean isForResult() {
        return forResult;
    }

    public int getCodeResult() {
        return codeResult;
    }
}