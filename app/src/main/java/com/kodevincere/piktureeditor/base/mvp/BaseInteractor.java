package com.kodevincere.piktureeditor.base.mvp;

import android.content.Context;

public abstract class BaseInteractor<P extends BasePresenterModel> {

    protected P presenterModel;

    public Context context;

    public BaseInteractor(Context context) {
        this.context = context;
    }

    public void setPresenterModel(P presenterModel) {
        this.presenterModel = presenterModel;
    }
}