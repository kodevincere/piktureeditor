package com.kodevincere.piktureeditor.base.recycleview;

import android.support.v7.widget.RecyclerView;

/**
 * Created by James
 */
public abstract class BaseAdapter<VH extends BaseViewHolder> extends RecyclerView.Adapter<VH> {

    protected OnActionAdapter actionAdapter;

    public final void addActionAdapter(OnActionAdapter actionAdapter) {
        this.actionAdapter = actionAdapter;
    }

    public void update() {
        notifyDataSetChanged();
    }
}
