package com.kodevincere.piktureeditor.base.util;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseJSON {
    protected JSONObject json;

    public void add(String key, boolean value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void add(String key, int value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void add(String key, long value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void add(String key, float value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void add(String key, double value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void add(String key, String value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void add(String key, Object value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
