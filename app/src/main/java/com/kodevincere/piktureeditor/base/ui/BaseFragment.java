package com.kodevincere.piktureeditor.base.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kodevincere.piktureeditor.base.mvp.BasePresenter;
import com.kodevincere.piktureeditor.base.util.KeyboardUtil;

import butterknife.ButterKnife;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements BaseViewModel {

    private BaseResources baseResources;
    protected P presenter;
    protected OnManageNavigation onManageNavigation;
    private Context context;

    public BaseFragment() {
    }

    public abstract int getViewLayout();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            onManageNavigation = (OnManageNavigation) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = startPresenter();
        if (presenter != null)
            presenter.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (presenter != null)
            presenter.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.resume();
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getViewLayout(), container, false);
        ButterKnife.bind(this, view);

        baseResources = getBaseResources();
        if (baseResources != null) ButterKnife.bind(baseResources, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (presenter != null)
            presenter.activityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null)
            presenter.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null)
            presenter.destroy();

        ButterKnife.unbind(this);
        if (baseResources != null)
            ButterKnife.unbind(baseResources);

    }

    @Override
    public final Context getViewContext() {
        return context;
    }

    public P startPresenter() {
        return null;
    }

    public BaseResources getBaseResources() {
        return BaseResources.nill;
    }

    public void onBundleForMe() {

    }

    public final void hideKeyboard() {
        KeyboardUtil.hide(this);
    }

}
