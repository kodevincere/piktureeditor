package com.kodevincere.piktureeditor.base.mvp;

import android.content.Context;
import android.content.Intent;

import com.kodevincere.piktureeditor.base.ui.BaseViewModel;
import com.kodevincere.piktureeditor.base.util.NetworkState;

public abstract class BasePresenter<V extends BaseViewModel> {

    protected V viewModel;
    protected Context context;
    private boolean callViewModel = true;

    public BasePresenter(V viewModel) {
        this.viewModel = viewModel;
        this.context = viewModel.getViewContext().getApplicationContext();
    }

    public void create() {
    }

    public void start() {
    }

    public void resume() {
    }

    public void activityResult(int requestCode, int resultCode, Intent intentData) {
    }

    public void pause() {
    }

    public void stop() {
    }

    public void destroy() {
    }

    public boolean checkInternetConnection() {
        return NetworkState.isNetworkConnected(context);
    }

    public boolean canCallView() {
        return callViewModel;
    }
}
