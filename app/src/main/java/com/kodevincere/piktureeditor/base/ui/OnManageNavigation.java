package com.kodevincere.piktureeditor.base.ui;

import android.os.Bundle;

import com.kodevincere.piktureeditor.base.launcher.LauncherViews;

/**
 * Created by James
 */
public interface OnManageNavigation {
    void onShowView(LauncherViews launcherViews);

    void onSaveExtra(LauncherViews launcherViews, Bundle bundle);

    void setResultCode(LauncherViews launcherViews, int resultCode);

    void onChangeTitle(String title);

    void popBackStack();

    void onFinish();

    Bundle bundleForMe(LauncherViews launcherViews);
}
