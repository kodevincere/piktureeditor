package com.kodevincere.piktureeditor.base.recycleview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kodevincere.piktureeditor.base.recycleview.ActionPack.Action;
import com.kodevincere.piktureeditor.base.recycleview.ActionPack.OptionEnum;
import com.kodevincere.piktureeditor.base.ui.BaseResources;

import butterknife.ButterKnife;

/**
 * Created by James
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    private OnActionAdapter action;
    protected Context context;
    protected T item;
    private View itemView;

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.itemView = itemView;
        this.context = itemView.getContext();

        BaseResources baseResources = getBaseResources();
        if (baseResources != null) ButterKnife.bind(baseResources, itemView);
    }

    public BaseViewHolder(View itemView, OnActionAdapter action) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.itemView = itemView;
        this.context = itemView.getContext();
        this.action = action;

        BaseResources baseResources = getBaseResources();
        if (baseResources != null) ButterKnife.bind(baseResources, itemView);
    }

    public void bindHolder(T item) {
        this.item = item;
        this.clean();
    }

    public BaseResources getBaseResources() {
        return null;
    }

    public abstract void clean();

    public final void dispatchClick(View view) {
        dispatchClick(view, null);
    }

    public final void dispatchClick(ActionOptions option) {
        dispatchClick(itemView, option);
    }

    public final void dispatchClick(View view, ActionOptions option) {
        if (action != null) {
            ActionPack p = ActionPack.make((view != null) ? view.getId() : itemView.getId())
                    .viewType(getItemViewType())
                    .item(item)
                    .position(getAdapterPosition())
                    .action(Action.CLICK)
                    .option((option != null) ? option : OptionEnum.NONE);
            action.onActionAdapter(p);
        }
    }

    public final void dispatchLongClick(View view) {
        dispatchLongClick(view, null);
    }

    public final void dispatchLongClick(ActionOptions option) {
        dispatchClick(itemView, option);
    }

    public final void dispatchLongClick(View view, ActionOptions option) {
        if (action != null) {
            ActionPack p = ActionPack.make((view != null) ? view.getId() : itemView.getId())
                    .viewType(getItemViewType())
                    .item(item)
                    .position(getAdapterPosition())
                    .action(Action.LONGCLICK)
                    .option((option != null) ? option : OptionEnum.NONE);
            action.onActionAdapter(p);
        }
    }

    public final void dispatchTouch(View view) {
        dispatchTouch(view, null);
    }

    public final void dispatchTouch(ActionOptions option) {
        dispatchTouch(itemView, option);
    }

    public final void dispatchTouch(View view, ActionOptions option) {
        if (action != null) {
            ActionPack p = ActionPack.make((view != null) ? view.getId() : itemView.getId())
                    .viewType(getItemViewType())
                    .item(item)
                    .position(getAdapterPosition())
                    .action(Action.TOUCH)
                    .option((option != null) ? option : OptionEnum.NONE);
            action.onActionAdapter(p);
        }
    }


}